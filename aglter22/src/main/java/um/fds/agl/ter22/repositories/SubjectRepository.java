package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Subject;

public interface SubjectRepository extends UserBaseRepository<Subject>{

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    Subject save(@Param("subject") Subject subject);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void deleteById(@Param("id") Long id);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void delete(@Param("subject") Subject subject);
}
