package um.fds.agl.ter22.entities;

import javax.persistence.Entity;

@Entity
public class Subject extends UserTER{

    private String title;

    private String Teacher;

    private String secondaryTeacher;

    public Subject(String title, String Teacher){
        this.title = title;
        this.Teacher = Teacher;
    }
    public Subject(String title, String Teacher,String secondaryTeacher){
        this.title = title;
        this.Teacher = Teacher;
        this.secondaryTeacher = secondaryTeacher;
    }

    public Subject(long id, String title, String Teacher, String secondaryTeacher){
        setId(id);
        this.title = title;
        this.Teacher = Teacher;
        this.secondaryTeacher =secondaryTeacher;
    }

    public Subject() {}

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }
    public String getTeacher() { return Teacher; }
    public void setTeacher(String Teacher) { this.Teacher = Teacher; }

    public String getSecondaryTeacher() {
        return secondaryTeacher;
    }

    public void setSecondaryTeacher(String secondaryTeacher) {
        this.secondaryTeacher = secondaryTeacher;
    }
}

