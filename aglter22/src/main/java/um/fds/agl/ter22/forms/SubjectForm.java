package um.fds.agl.ter22.forms;

import um.fds.agl.ter22.entities.Student;

public class SubjectForm {
    private String title;
    private long id;
    private String teacher;

    private String secondaryTeacher;

    public SubjectForm(long id, String title, String teacher, String secondaryTeacher) {
        this.title = title;
        this.teacher = teacher;
        this.id = id;
        this.secondaryTeacher = secondaryTeacher;
    }

    public SubjectForm() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacherName) {
        this.teacher = teacherName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSecondaryTeacher() {
        return secondaryTeacher;
    }

    public void setSecondaryTeacher(String secondaryTeacher) {
        this.secondaryTeacher = secondaryTeacher;
    }
}
